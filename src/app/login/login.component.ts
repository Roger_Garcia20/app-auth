import { Component } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  isLogged: boolean = false;

  constructor(private route: Router) { }

  form: FormGroup = new FormGroup({
    user: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  initializeFormGroup() {
    this.form.setValue({
      user: '',
      email: '',
      password: '',
      confirmPassword: ''
    });
  }

  onClear() {
    this.form.reset();
    this.initializeFormGroup();
  }

  initSession({user, password}): void {

    // Se Guarda la informacion en el Local Storage
    localStorage.setItem("user", user);

    // Se verifica que el usuario tenga los permisos para el acceso
    if(user == 'dist03' && password == 'dist03') {
      alert('Inicio de sesion correcto');
      this.isLogged = true;
      this.route.navigate(['/dashboard']);
    } else {
      alert('Acceso denegado, Credenciales Incorrectas');
      this.isLogged = false;
    }
  }

}
