import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) {}

  getCountries(params = {}) {
    const URL = 'https://api.first.org/data/v1/countries?region=africa&limit=10&pretty=true';
    return this.http.get(URL, {params});
  }
  
}
