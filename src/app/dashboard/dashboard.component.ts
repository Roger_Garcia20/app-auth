import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { CountryService } from '../services/country.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  
  countries: Array<any> = [];
  hoy = new Date();
  day = 1000 * 60 * 60 * 24;
  month = 1000 * 60 * 60 * 24 * 30 * 7;
  suma = this.hoy.getTime() + this.day;
  resta = this.hoy.getTime() - this.month;
  f_inicial = new Date(this.resta);
  f_final = new Date(this.suma);
  min = new Date(this.f_inicial.getFullYear(), this.f_inicial.getMonth(), this.f_inicial.getDate());
  max = new Date(this.f_final.getFullYear(), this.f_final.getMonth(), this.f_final.getDate());


  constructor(private countryService: CountryService, private route: Router) { }

  ngOnInit(): void {
    this.dataCountry();
    this.getUser();
  }

  // Validacion de Formulario
  form: FormGroup = new FormGroup({
    fechaReserva: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    nombre: new FormControl('', Validators.required),
    pais: new FormControl('', Validators.required)
  });

  // Funcion para inicializar Formulario
  initializeFormGroup() {
    this.form.setValue({
      fechaReserva: '',
      email: '',
      nombre: '',
      pais:''
    });
  }

  // Funcion para resetear Formulario
  onClear() {
    this.form.reset();
    this.initializeFormGroup();
  }



  dataCountry() {
    this.countryService.getCountries().subscribe((res: any) => {
      this.countries = res.data.CV.country;
    });
  }

  sendData({email, nombre, pais}) {
    alert('Registrado!!.. Datos del Usuario : ' + ' ' + 'Email: ' + email + ' ' + 'Nombre Completo: ' + nombre + ' ' + 'Pais de Origen: ' + pais);
  }

  logout():void {
    alert('Sesion Finalizada con Exito!!');
    this.route.navigate(['/login']);
    localStorage.removeItem("user");
  }

  getUser() {
    const user = localStorage.getItem("user");
    console.log(user);
  }

  /* validEmail(email) {
    let er = new RegExp('(\W|^)[\w.\-]{0,25}@(yahoo|hotmail|gmail)\.com(\W|$)');
    
    }
  } */

}
