export interface Usuario {
    name: string,
    password: string,
    passwordRepeat: string
}